const Sequelize = require('sequelize');
const db = new Sequelize(
  process.env.DATABASE_URL ||
    'postgres://postgres:secret@localhost:5432/postgres'
);

db.sync({ force: false })
  .then(() => console.log('DB schema updated'))
  .catch(err => console.log(err));
module.exports = db;
