const Sequelize = require('sequelize');
const db = require('../db');
const Worklog = require('../worklog/model');

const Task = db.define('task', {
  name: {
    type: Sequelize.STRING,
    allowNull: false
  },
  startDate: {
    type: Sequelize.DATEONLY,
    allowNull: false
  },
  endDate: {
    type: Sequelize.DATEONLY
  },
  complexity: {
    type: Sequelize.ENUM('High', 'Medium', 'Low'),
    allowNull: false
  },
  status: {
    type: Sequelize.ENUM('In Progress', 'Completed'),
    allowNull: false
  }
});
Task.hasMany(Worklog);
Worklog.belongsTo(Task);

module.exports = Task;
