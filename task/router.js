const { Router } = require('express');
const Task = require('./model');
const Worklog = require('../worklog/model');

const router = new Router();

router.post('/task', (req, res, next) => {
  const task = req.body;

  Task.create(task)
    .then(task => res.json(task))
    .catch(err => next(err));
});

router.get('/task', (req, res, next) => {
  Task.findAll({
    /* where: {
      status: 'In Progress'
    } */
  }).then(tasks => res.json(tasks));
});

router.patch('/task/:id/toggleStatus', (req, res, next) => {
  const taskId = parseInt(req.params.id);
  const { date } = req.body;

  Task.findOne({
    where: {
      id: taskId
    }
  }).then(task => {
    task.status === 'In Progress'
      ? (task.status = 'Completed')
      : (task.status = 'In Progress');
    if (task.status === 'Completed') {
      return task
        .update({
          endDate: date,
          status: task.status
        })
        .then(task => res.json(task))
        .catch(err => next(err));
    }
    task
      .update({
        status: task.status
      })
      .then(task => res.json(task))
      .catch(err => next(err));
  });
});

module.exports = router;
