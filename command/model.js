const Sequelize = require('sequelize');
const db = require('../db');
const Command = db.define('command', {
  subject: {
    type: Sequelize.TEXT,
    allowNull: false
  },
  command: {
    type: Sequelize.TEXT,
    defaultValue: 'None'
  },
  otherInfo: {
    type: Sequelize.TEXT,
    defaultValue: 'None'
  }
});

module.exports = Command;
