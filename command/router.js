const { Router } = require('express');
const router = new Router();
const Command = require('./model');
router.post('/command', (req, res, next) => {
  const command = req.body;
  Command.create(command)
    .then(cmd => res.json(cmd))
    .catch(err => next(err));
});
router.get('/commands', (req, res, next) => {
  Command.findAll()
    .then(cmds => res.json(cmds))
    .catch(err => next(err));
});
router.patch('/command/:id', async (req, res, next) => {
  const id = parseInt(req.params.id);
  const { subject, command, otherInfo } = req.body;
  const commandResult = await Command.findOne({ where: { id } });
  const updatedCommand = await commandResult.update({
    subject: subject,
    command: command,
    otherInfo: otherInfo
  });
  const result = res.json(updatedCommand);
});

router.delete('/command/:id', (req, res, next) => {
  const id = parseInt(req.params.id);

  Command.destroy({ where: { id } })
    .then(cnt => res.json(cnt))
    .catch(err => next(err));
});
module.exports = router;
