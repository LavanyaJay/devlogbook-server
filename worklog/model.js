const Sequelize = require('sequelize');
const db = require('../db');
const Worklog = db.define('worklog', {
  date: {
    type: Sequelize.DATEONLY,
    allowNull: false
  },
  duration: {
    type: Sequelize.TIME,
    allowNull: false
  },
  notes: {
    type: Sequelize.TEXT,
    allowNull: false
  },

  lessonsLearnt: {
    type: Sequelize.TEXT,
    defaultValue: 'None'
  }
});
module.exports = Worklog;
