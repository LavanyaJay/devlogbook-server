const { Router } = require('express');
const Sequelize = require('sequelize');
const Worklog = require('./model');
const router = new Router();
const Task = require('../task/model');
const moment = require('moment');
router.post('/worklog', async (req, res, next) => {
  const worklog = req.body;
  Worklog.create(worklog, {
    include: [
      {
        model: Task,
        attributes: ['name', 'status', 'startDate', 'endDate', 'complexity'],
        where: { id: req.body.taskId }
      }
    ]
  })
    .then(newWorkLog => {
      return newWorkLog.reload();
    })
    .then(workLog => {
      console.log(workLog);
      res.json(workLog);
    })
    .catch(err => next(err));
});

router.get('/worklog/:date', (req, res, next) => {
  const date = req.params.date;

  Worklog.findAll({
    attributes: ['taskId', 'notes', 'lessonsLearnt', 'date', 'id'],
    include: [
      {
        model: Task
      }
    ],
    where: {
      date: date
    },
    group: ['taskId', 'task.id', 'notes', 'lessonsLearnt', 'date', 'worklog.id']
  })
    .then(worklog => res.json(worklog))
    .catch(err => next(err));
});

router.get('/worklog/:taskId/notes', (req, res, next) => {
  Worklog.findAll({
    where: {
      taskId: req.params.taskId
    },
    attributes: ['notes', 'date', 'id']
  })
    .then(note => res.json(note))
    .catch(err => next(err));
});

router.get('/getChart', (req, res, next) => {
  Worklog.findAll({
    attributes: [
      'taskId',
      [Sequelize.fn('SUM', Sequelize.col('duration')), 'TotalDuration']
    ],
    include: [
      {
        model: Task,
        attributes: ['name']
      }
    ],
    group: ['taskId', 'task.id', 'task.name']
  })

    .then(data => res.json(data))
    .catch(err => next(err));
});

router.get('/totalDuration/:taskId', (req, res, next) => {
  Worklog.findOne({
    attributes: [
      'taskId',
      [Sequelize.fn('SUM', Sequelize.col('duration')), 'TotalDuration']
    ],
    include: [
      {
        model: Task,
        attributes: ['name']
      }
    ],
    where: {
      taskId: req.params.taskId
    },
    group: ['taskId', 'task.id', 'task.name']
  })

    .then(data => res.json(data))
    .catch(err => next(err));
});

router.patch('/worklog/:id', (req, res, next) => {
  Worklog.findOne({
    where: {
      id: req.params.id
    },
    attributes: ['notes', 'date', 'id']
  })
    .then(log =>
      log
        .update({ notes: req.body.notes })
        .then(notes => res.json(notes))
        .catch(err => next(err))
    )
    .catch(err => next(err));
});
module.exports = router;
