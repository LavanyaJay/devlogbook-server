const express = require('express');
const app = express();
const port = process.env.PORT || 4000;

//Adding middlewares
const cors = require('cors');
const corsMiddleware = cors();

const bodyParser = require('body-parser');
const jsonMiddleware = bodyParser.json();

app.use(corsMiddleware);
app.use(jsonMiddleware);

//Adding DB Models
const Reflection = require('./reflection/model');
const Worklog = require('./worklog/model');
const Command = require('./command/model');
const Task = require('./task/model');

//Adding routers
const reflectionsRouter = require('./reflection/router');
app.use(reflectionsRouter);

const worklogsRouter = require('./worklog/router');
app.use(worklogsRouter);

const commandsRouter = require('./command/router');
app.use(commandsRouter);

const tasksRouter = require('./task/router');
app.use(tasksRouter);

app.listen(port, () => console.log('Listening on port:' + port));
