const { Router } = require('express');
const router = new Router();
const Reflection = require('./model');

router.post('/reflection', (req, res, next) => {
  const reflection = req.body;
  Reflection.create(reflection)
    .then(reflection => res.json(reflection))
    .catch(next);
});

router.get('/reflection', (req, res, next) => {
  Reflection.findAll()
    .then(reflection => res.json(reflection))
    .catch(next);
});
module.exports = router;
