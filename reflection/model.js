const db = require('../db');
const Sequelize = require('sequelize');
const Reflection = db.define('reflection', {
  subject: {
    type: Sequelize.TEXT,
    allowNull: false
  },
  thoughts: {
    type: Sequelize.TEXT,
    allowNull: false
  }
});

module.exports = Reflection;
